# Backyard Rover

## Ideas

* AMG8833 thermal camera
* HC-S04 ultrasonic sensor
* Tank Tracks [Amazon](https://www.amazon.ca/Lightweight-Absorbed-Plastic-Platform-Recording/dp/B076MBQMHH)
* Dual-Servo Armature for sensors
* Text-to-Speech
* RMF9x LoRa radio broadcasting
* Pi camera [Amazon](https://www.amazon.ca/Smraza-Raspberry-Camera-Module-Supports/dp/B076KCZRDS)
* Gripper hand [Thingiverse](https://www.thingiverse.com/thing:71797)

## Tools

* 3D printer [Amazon](https://www.amazon.ca/Official-Creality-3D-Ender-Printer/dp/B07CJ5PFVP/ref=pd_sbs_328_1/136-6900664-4480018)
